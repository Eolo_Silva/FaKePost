﻿namespace FakePost
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxStatusNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxStatusText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxStatusDateTime = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxCommenttext = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxCommentNome = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxCommentDateTime = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.lblContador2Comentario = new System.Windows.Forms.Label();
            this.textBoxCommentPost = new System.Windows.Forms.TextBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.lblPostCommentDateTime = new System.Windows.Forms.Label();
            this.lblPostTituloComentario = new System.Windows.Forms.Label();
            this.LblPostCommentNome = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblPostAcumulaNomesLike = new System.Windows.Forms.Label();
            this.lblContadorLikes = new System.Windows.Forms.Label();
            this.lblPostLikeTeuNome = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lblPostDateTime = new System.Windows.Forms.Label();
            this.linkpostlblComment = new System.Windows.Forms.LinkLabel();
            this.linkpostlblLike = new System.Windows.Forms.LinkLabel();
            this.lblPostStatusText = new System.Windows.Forms.Label();
            this.lblPostStatusNome = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name :";
            // 
            // textBoxStatusNome
            // 
            this.textBoxStatusNome.Location = new System.Drawing.Point(83, 73);
            this.textBoxStatusNome.Name = "textBoxStatusNome";
            this.textBoxStatusNome.Size = new System.Drawing.Size(195, 20);
            this.textBoxStatusNome.TabIndex = 1;
            this.textBoxStatusNome.TextChanged += new System.EventHandler(this.textBoxStatusNome_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Status Text :";
            // 
            // textBoxStatusText
            // 
            this.textBoxStatusText.Location = new System.Drawing.Point(83, 97);
            this.textBoxStatusText.Multiline = true;
            this.textBoxStatusText.Name = "textBoxStatusText";
            this.textBoxStatusText.Size = new System.Drawing.Size(320, 39);
            this.textBoxStatusText.TabIndex = 3;
            this.textBoxStatusText.TextChanged += new System.EventHandler(this.textBoxStatusText_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Status";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Picture Profile :";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MidnightBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(82, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "Upload Image";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DarkGray;
            this.label5.Location = new System.Drawing.Point(201, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "(.jpg .jpeg or .png)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 174);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Date/Time :";
            // 
            // textBoxStatusDateTime
            // 
            this.textBoxStatusDateTime.Location = new System.Drawing.Point(83, 170);
            this.textBoxStatusDateTime.Name = "textBoxStatusDateTime";
            this.textBoxStatusDateTime.Size = new System.Drawing.Size(169, 20);
            this.textBoxStatusDateTime.TabIndex = 17;
            this.textBoxStatusDateTime.Text = "--/--/----";
            this.textBoxStatusDateTime.TextChanged += new System.EventHandler(this.textBoxStatusDateTime_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(9, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Format";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(90, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Classic :";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(134, 14);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(14, 13);
            this.radioButton1.TabIndex = 20;
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(155, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Timeline :";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.radioButton2.Location = new System.Drawing.Point(203, 14);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(14, 13);
            this.radioButton2.TabIndex = 22;
            this.radioButton2.TabStop = true;
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.LightGray;
            this.label15.Location = new System.Drawing.Point(9, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(421, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "_____________________________________________________________________";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.LightGray;
            this.label16.Location = new System.Drawing.Point(10, 196);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(421, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "_____________________________________________________________________";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 216);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 25;
            this.label17.Text = "Comment";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.DarkGray;
            this.label18.Location = new System.Drawing.Point(200, 311);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 32;
            this.label18.Text = "(.jpg .jpeg or .png)";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.MidnightBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(81, 305);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 27);
            this.button2.TabIndex = 31;
            this.button2.Text = "Upload Image";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 310);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Picture Profile :";
            // 
            // textBoxCommenttext
            // 
            this.textBoxCommenttext.Location = new System.Drawing.Point(82, 261);
            this.textBoxCommenttext.Multiline = true;
            this.textBoxCommenttext.Name = "textBoxCommenttext";
            this.textBoxCommenttext.Size = new System.Drawing.Size(320, 39);
            this.textBoxCommenttext.TabIndex = 29;
            this.textBoxCommenttext.TextChanged += new System.EventHandler(this.textBoxCommenttext_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1, 264);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "Comment Text :";
            // 
            // textBoxCommentNome
            // 
            this.textBoxCommentNome.Location = new System.Drawing.Point(82, 237);
            this.textBoxCommentNome.Name = "textBoxCommentNome";
            this.textBoxCommentNome.Size = new System.Drawing.Size(195, 20);
            this.textBoxCommentNome.TabIndex = 27;
            this.textBoxCommentNome.TextChanged += new System.EventHandler(this.textBoxCommentNome_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(42, 240);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Name :";
            // 
            // textBoxCommentDateTime
            // 
            this.textBoxCommentDateTime.Location = new System.Drawing.Point(81, 338);
            this.textBoxCommentDateTime.Name = "textBoxCommentDateTime";
            this.textBoxCommentDateTime.Size = new System.Drawing.Size(169, 20);
            this.textBoxCommentDateTime.TabIndex = 34;
            this.textBoxCommentDateTime.Text = "00/00/0000";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(18, 342);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Date/Time :";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.MidnightBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(82, 381);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 27);
            this.button3.TabIndex = 39;
            this.button3.Text = "Add Comment";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.LightGray;
            this.label25.Location = new System.Drawing.Point(4, 365);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(421, 13);
            this.label25.TabIndex = 40;
            this.label25.Text = "_____________________________________________________________________";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.lblPostDateTime);
            this.groupBox1.Controls.Add(this.linkpostlblComment);
            this.groupBox1.Controls.Add(this.linkpostlblLike);
            this.groupBox1.Controls.Add(this.lblPostStatusText);
            this.groupBox1.Controls.Add(this.lblPostStatusNome);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(464, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 222);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Location = new System.Drawing.Point(70, 81);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(209, 124);
            this.panel2.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.lblContador2Comentario);
            this.panel1.Controls.Add(this.textBoxCommentPost);
            this.panel1.Controls.Add(this.linkLabel4);
            this.panel1.Controls.Add(this.lblPostCommentDateTime);
            this.panel1.Controls.Add(this.lblPostTituloComentario);
            this.panel1.Controls.Add(this.LblPostCommentNome);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.lblPostAcumulaNomesLike);
            this.panel1.Controls.Add(this.lblContadorLikes);
            this.panel1.Controls.Add(this.lblPostLikeTeuNome);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 113);
            this.panel1.TabIndex = 8;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(45, 62);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 13;
            this.label27.Text = "Gostar ";
            // 
            // lblContador2Comentario
            // 
            this.lblContador2Comentario.AutoSize = true;
            this.lblContador2Comentario.Location = new System.Drawing.Point(139, 62);
            this.lblContador2Comentario.Name = "lblContador2Comentario";
            this.lblContador2Comentario.Size = new System.Drawing.Size(10, 13);
            this.lblContador2Comentario.TabIndex = 12;
            this.lblContador2Comentario.Text = "-";
            // 
            // textBoxCommentPost
            // 
            this.textBoxCommentPost.ForeColor = System.Drawing.Color.Gainsboro;
            this.textBoxCommentPost.Location = new System.Drawing.Point(7, 84);
            this.textBoxCommentPost.Name = "textBoxCommentPost";
            this.textBoxCommentPost.Size = new System.Drawing.Size(186, 20);
            this.textBoxCommentPost.TabIndex = 11;
            this.textBoxCommentPost.Text = " Write a comment...";
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.LinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.linkLabel4.Location = new System.Drawing.Point(156, 63);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(27, 13);
            this.linkLabel4.TabIndex = 9;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Like";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // lblPostCommentDateTime
            // 
            this.lblPostCommentDateTime.AutoSize = true;
            this.lblPostCommentDateTime.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblPostCommentDateTime.Location = new System.Drawing.Point(155, 33);
            this.lblPostCommentDateTime.Name = "lblPostCommentDateTime";
            this.lblPostCommentDateTime.Size = new System.Drawing.Size(38, 13);
            this.lblPostCommentDateTime.TabIndex = 9;
            this.lblPostCommentDateTime.Text = "10 min";
            // 
            // lblPostTituloComentario
            // 
            this.lblPostTituloComentario.AutoSize = true;
            this.lblPostTituloComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostTituloComentario.Location = new System.Drawing.Point(42, 47);
            this.lblPostTituloComentario.Name = "lblPostTituloComentario";
            this.lblPostTituloComentario.Size = new System.Drawing.Size(154, 13);
            this.lblPostTituloComentario.TabIndex = 9;
            this.lblPostTituloComentario.Text = "Type your Fake comment here.";
            // 
            // LblPostCommentNome
            // 
            this.LblPostCommentNome.AutoSize = true;
            this.LblPostCommentNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPostCommentNome.ForeColor = System.Drawing.SystemColors.Highlight;
            this.LblPostCommentNome.Location = new System.Drawing.Point(43, 32);
            this.LblPostCommentNome.Name = "LblPostCommentNome";
            this.LblPostCommentNome.Size = new System.Drawing.Size(91, 13);
            this.LblPostCommentNome.TabIndex = 9;
            this.LblPostCommentNome.Text = "Commenter Name";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(3, 31);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 41);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // lblPostAcumulaNomesLike
            // 
            this.lblPostAcumulaNomesLike.AutoSize = true;
            this.lblPostAcumulaNomesLike.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblPostAcumulaNomesLike.Location = new System.Drawing.Point(115, 9);
            this.lblPostAcumulaNomesLike.Name = "lblPostAcumulaNomesLike";
            this.lblPostAcumulaNomesLike.Size = new System.Drawing.Size(36, 13);
            this.lblPostAcumulaNomesLike.TabIndex = 3;
            this.lblPostAcumulaNomesLike.Text = "others";
            this.lblPostAcumulaNomesLike.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblContadorLikes
            // 
            this.lblContadorLikes.AutoSize = true;
            this.lblContadorLikes.Location = new System.Drawing.Point(154, 9);
            this.lblContadorLikes.Name = "lblContadorLikes";
            this.lblContadorLikes.Size = new System.Drawing.Size(42, 13);
            this.lblContadorLikes.TabIndex = 2;
            this.lblContadorLikes.Text = "like this";
            // 
            // lblPostLikeTeuNome
            // 
            this.lblPostLikeTeuNome.AutoSize = true;
            this.lblPostLikeTeuNome.Location = new System.Drawing.Point(28, 9);
            this.lblPostLikeTeuNome.Name = "lblPostLikeTeuNome";
            this.lblPostLikeTeuNome.Size = new System.Drawing.Size(47, 13);
            this.lblPostLikeTeuNome.TabIndex = 1;
            this.lblPostLikeTeuNome.Text = "You and";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 19);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label33.Location = new System.Drawing.Point(3, 15);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(193, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "_______________________________";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label37.Location = new System.Drawing.Point(4, 66);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(193, 13);
            this.label37.TabIndex = 10;
            this.label37.Text = "_______________________________";
            // 
            // lblPostDateTime
            // 
            this.lblPostDateTime.AutoSize = true;
            this.lblPostDateTime.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblPostDateTime.Location = new System.Drawing.Point(173, 61);
            this.lblPostDateTime.Name = "lblPostDateTime";
            this.lblPostDateTime.Size = new System.Drawing.Size(65, 13);
            this.lblPostDateTime.TabIndex = 7;
            this.lblPostDateTime.Text = "29/09/2017";
            // 
            // linkpostlblComment
            // 
            this.linkpostlblComment.AutoSize = true;
            this.linkpostlblComment.LinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.linkpostlblComment.Location = new System.Drawing.Point(116, 60);
            this.linkpostlblComment.Name = "linkpostlblComment";
            this.linkpostlblComment.Size = new System.Drawing.Size(51, 13);
            this.linkpostlblComment.TabIndex = 6;
            this.linkpostlblComment.TabStop = true;
            this.linkpostlblComment.Text = "Comment";
            // 
            // linkpostlblLike
            // 
            this.linkpostlblLike.AutoSize = true;
            this.linkpostlblLike.LinkColor = System.Drawing.SystemColors.ActiveCaption;
            this.linkpostlblLike.Location = new System.Drawing.Point(65, 60);
            this.linkpostlblLike.Name = "linkpostlblLike";
            this.linkpostlblLike.Size = new System.Drawing.Size(27, 13);
            this.linkpostlblLike.TabIndex = 4;
            this.linkpostlblLike.TabStop = true;
            this.linkpostlblLike.Text = "Like";
            this.linkpostlblLike.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkpostlblLike_LinkClicked);
            // 
            // lblPostStatusText
            // 
            this.lblPostStatusText.AutoSize = true;
            this.lblPostStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostStatusText.Location = new System.Drawing.Point(65, 43);
            this.lblPostStatusText.Name = "lblPostStatusText";
            this.lblPostStatusText.Size = new System.Drawing.Size(103, 16);
            this.lblPostStatusText.TabIndex = 3;
            this.lblPostStatusText.Text = "Your fake status";
            // 
            // lblPostStatusNome
            // 
            this.lblPostStatusNome.AutoSize = true;
            this.lblPostStatusNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostStatusNome.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPostStatusNome.Location = new System.Drawing.Point(63, 22);
            this.lblPostStatusNome.Name = "lblPostStatusNome";
            this.lblPostStatusNome.Size = new System.Drawing.Size(122, 20);
            this.lblPostStatusNome.TabIndex = 2;
            this.lblPostStatusNome.Text = "Your fake name";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label26.Location = new System.Drawing.Point(67, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(0, 13);
            this.label26.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 514);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBoxCommentDateTime);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBoxCommenttext);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBoxCommentNome);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxStatusDateTime);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxStatusText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxStatusNome);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fake Facebook Post";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxStatusNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxStatusText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxStatusDateTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxCommenttext;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxCommentNome;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxCommentDateTime;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPostStatusNome;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblPostDateTime;
        private System.Windows.Forms.LinkLabel linkpostlblComment;
        private System.Windows.Forms.LinkLabel linkpostlblLike;
        private System.Windows.Forms.Label lblPostStatusText;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxCommentPost;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Label lblPostCommentDateTime;
        private System.Windows.Forms.Label lblPostTituloComentario;
        private System.Windows.Forms.Label LblPostCommentNome;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblPostAcumulaNomesLike;
        private System.Windows.Forms.Label lblContadorLikes;
        private System.Windows.Forms.Label lblPostLikeTeuNome;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblContador2Comentario;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label27;
    }
}

