﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FakePost
{
    public partial class Form1 : Form
    {
        Post post = new FakePost.Post();
        int count, count2;
        public Form1()
        {
            InitializeComponent();
            post.LerInfo();
            lblPostStatusNome.Text = post.StatusNome;
            lblPostStatusText.Text = post.StatusStatus;
            LblPostCommentNome.Text = post.CommentNome;
            lblPostTituloComentario.Text = post.CommentStatus;
            lblContadorLikes.Text = post.ContadorLike;
            lblContador2Comentario.Text = post.Contador2Like;

        }

        private void textBoxStatusNome_TextChanged(object sender, EventArgs e)
        {
            post.StatusNome = textBoxStatusNome.Text;
            lblPostStatusNome.Text = post.StatusNome;
        }

        private void textBoxStatusText_TextChanged(object sender, EventArgs e)
        {
            textBoxStatusNome.Enabled = false;
            post.StatusStatus = textBoxStatusText.Text;
            lblPostStatusText.Text = post.StatusStatus;
        }

        private void textBoxStatusDateTime_TextChanged(object sender, EventArgs e)
        {
            textBoxStatusText.Enabled = false;
            lblPostDateTime.Text = textBoxStatusDateTime.Text;
        }

        private void textBoxCommentNome_TextChanged(object sender, EventArgs e)
        {
            textBoxStatusDateTime.Enabled = false;
            post.CommentNome = textBoxCommentNome.Text;
            LblPostCommentNome.Text = post.CommentNome;
        }

        private void textBoxCommenttext_TextChanged(object sender, EventArgs e)
        {
            textBoxCommentNome.Enabled = false;
            post.CommentStatus = textBoxCommenttext.Text;
            lblPostTituloComentario.Text = post.CommentStatus;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "jpg files (*.jpg)|*.jpg";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Image = new Bitmap(dlg.FileName);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "jpg files (*.jpg)|*.jpg";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    pictureBox3.Image = new Bitmap(dlg.FileName);
                }
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            panel2.TabIndex = 0;
            if (!panel2.Focused)
            {
                panel2.Focus();
            }
        }

        private void linkpostlblLike_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            lblPostLikeTeuNome.Text = LblPostCommentNome.Text;
            if(LblPostCommentNome.Text != lblPostStatusNome.Text)
            {
                count++;
            }
            lblContadorLikes.Text = Convert.ToString(count);
            post.ContadorLike = Convert.ToString(count);
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            label27.Text = lblPostStatusNome.Text;
            if (lblPostStatusNome.Text != LblPostCommentNome.Text)
            {
                count2++;
            }
            lblContador2Comentario.Text = Convert.ToString(count2);
            post.Contador2Like = Convert.ToString(count2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            post.GravarInfo();
        }
    }
}