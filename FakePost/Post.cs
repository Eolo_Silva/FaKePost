﻿using System;
using System.IO;

namespace FakePost
{
    class Post
    {
        #region Atributos
        private string _statusNome;
        private string _statusStatus;
        private DateTime _statusData;
        private string _commentNome;
        private string _commentStatus;
        private DateTime _commentData;
        private string _contadorLike;
        private string _contador2Like;
        #endregion

        #region propriedades
        public string StatusNome { get; set; }
        public string StatusStatus { get; set; }
        public DateTime StatusData { get; set; }
        public string CommentNome { get; set; }
        public string CommentStatus { get; set; }
        public DateTime CommentData { get; set; }
        public string ContadorLike { get; set; }
        public string Contador2Like { get; set; }
        #endregion

        #region Construtores
        //public Post()
        //{
        //    StatusNome = "Eolo Guerra Silva";
        //    StatusStatus = "Adicionou 2 novas fotografias";
        //    StatusData = DateTime.Now;
        //}

        public Post() : this("","",Convert.ToDateTime("19/09/2017")) { }

        public Post(string statusNome, string statusStatus, DateTime statusData)
        {
            StatusNome = statusNome;
            StatusStatus = statusStatus;
            StatusData = statusData;
        }
        public Post(Post FP) : this(FP.StatusNome, FP.StatusStatus, FP.StatusData)
        {
            
        }
        #endregion

        #region Metodos Gerais
        public override string ToString()
        {
            return String.Format("O Post de {0}, Status {1} na data:{2} foi realizado com SUCESSO!", StatusNome, StatusStatus, Convert.ToDateTime(StatusData));
        }
        #endregion

        #region Outros Metodos
        public void GravarInfo()
        {
            string ficheiro = @"post.txt";

            string linha = StatusNome + ";" + StatusStatus +  ";" + ContadorLike + ";" + CommentNome + ";" + CommentStatus + ";" + Contador2Like;

            StreamWriter sw = new StreamWriter(ficheiro, false);

            if (!File.Exists(ficheiro))
            {
                sw = File.CreateText(ficheiro);
            }

            sw.WriteLine(linha);
            sw.Close();
        }

        public void LerInfo()
        {
            string ficheiro = @"post.txt";

            StreamReader sr;

            if (File.Exists(ficheiro))
            {
                sr = File.OpenText(ficheiro);

                string linha = "";

                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = new string[5];

                    campos = linha.Split(';');

                    StatusNome = (campos[0]);
                    StatusStatus = (campos[1]);
                    ContadorLike = (campos[2]);
                    CommentNome = (campos[3]);
                    CommentStatus = (campos[4]);
                    Contador2Like = (campos[5]);
                }
                sr.Close();
            }
        }


        #endregion
    }
}